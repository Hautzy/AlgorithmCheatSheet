# Google application cheat sheet

This is a cheat sheet containing most of the important information / data / technologies you must know when applying at google.

## Basic knowledge

### Time and space complexity

Time and space complexity helps us to compare different approaches to solve a problem. It helps us to find the best algorithm for a problem.

* __Time complexity__ measures the time taken by an algorithm based on the length of the input
* __Space complexity__ measures the space or memory by an algorithm base on the length of the input

* __Order of grow__ measures how the time and space complexity of an algorithm scale with increasing or decreasing input length

* __O-notation__ To denote asymptotic upper bound(upper limit / execution time in the worst case), we use O-notation. For a given function g(n), we denote by O(g(n)) (pronounced "big-oh of g of n")

#### Big-O Notation closer look

The time efficiency of many algorithms can be characterized by only a few growth rate functions:

* __O(1) - constant time__ <br>
    This means that the algorithm requires the same fixed number of steps regardless of the length of the input.
    * Push and Pop operations for a stack
    * Insert and Remove operations for a queue

* __O(n) - linear time__ <br>
    This means that the algorithm requires a number of steps proportional with the length of the input.
    * Looping through an array
    * Finding the maximum or minimum element in a list
    * Sequential search in an unsorted list of n elements
    * Traversal of a tree with n elements
    * Determine n-factorial
    * Iteratevly finding the nth Fibonacci number

* __O(n<sup>2</sup>) - quadratic time__ <br>
    The number of operations performed is the square of the length of the input.
    * Some sorting algorithms, for example selection sort
    * Comparing two two-dimensional arrasys, both of size n
    * Finding duplicates in two unsorted arrays with nested loops

* __O(log n) - logarithmic time__ <br>
    When using logarithmic growth the base of the logarithm is not important when comparing for example O(log<sub>2</sub> n) and O(log<sub>10</sub> n).

    * Binary search in a sorted list of n elements
    * Insert and Find operations for a binary search tree with n elements
    * Insert and Remove operations for a heap with n elements

* __O(n log n) - "n log n" time__
    * More advanced sorting algorthms like quicksort or mergesort

* __O(a<sup>n</sup>) (a > 1) - exponential time__
    * Recursive Fibonacci implementation
    * Towers of Hanoi
    * Generating all permutations of n symbols

All in all we can say:

__O(1) < O(log n) < O(n) < O(n log n) < O(n<sup>2</sup>) < O(n<sup>3</sup>) < O(n<sup>a</sup>)__

If a function describing the time complexity of an algorithm, is a sum of several termas its order of growth is determined by the fastest growing term.

Two examples:

1. a<sub>1</sub>n + a<sub>2</sub>n + a<sub>k</sub>n<sup>k</sup> - so the function is O(n<sup>k</sup>)

2. ![](https://gyazo.com/8ab691481160887c09e784ca8e5e7140.png)

The table below helps you to understand the relation between length of input and common time complexities.

![](https://gyazo.com/da58ce84c3164c8cf2d108bc06034ea6.png)

Now lets go over some examples:

![](https://gyazo.com/1e750fd3bf12123bc4570e5e08e5e92c.png)

* This algorithms runs in O(n) where n is the size of the array.
* The number of elementary steps is ~ n.

![](https://gyazo.com/d794712067bff12fd829335574f1b383.png)

* This algorithm runs in O(n<sup>2</sup>).
* The number of elementary steps is ~ n*(n+1) / 2.

![](https://gyazo.com/8836c52ea1ec14cc2c3f2b05cad3dbc0.png)

* This algorithm runs in O(n<sup>3</sup>).
* The number of elementary steps is ~ n<sup>3</sup>.

![](https://gyazo.com/96153972b52b161a32aa6f560a7a32cf.png)

* This algorithm runs in O(n*m).
* The number of elementary steps is ~ n*m.

![](https://gyazo.com/e7c346d58f182b3d795832500176a5b4.png)

* This algorithm runs in O(n*m).
* The number of elementary steps is ~ n*m + min(m, n) * n.

![](https://gyazo.com/397d1c7f1e6c4e6115fce953eb3a6b84.png)

* This algorithm runs in O(n).
* The number of elementary steps is ~ n.

![](https://gyazo.com/8a0c16a9efbf06b541b3fb2108a3eb8f.png)

* This algorithm runs in O(2<sup>n</sup>).
* The number of elementary steps is ~ Fib(n+1) where Fib(k) is the k-th Fibonacci's number.

Further information on Data Structures and their time and space complexity can be found [here](https://de.slideshare.net/introprogramming/19-algorithms-andcomplexity?qid=92fd5b65-fa05-4318-9df5-c878f6a2c00a&v=&b=&from_search=3).

Sources:

* [Time and space complexity explained](https://www.hackerearth.com/practice/basic-programming/complexity-analysis/time-and-space-complexity/tutorial/)
* [O-Notation and several time complexities](https://de.slideshare.net/ANKKATIYAR/time-and-space-complexity)
* [Very good presentation of time and space complexity](https://de.slideshare.net/introprogramming/19-algorithms-andcomplexity?qid=92fd5b65-fa05-4318-9df5-c878f6a2c00a&v=&b=&from_search=3)

## Algorithms

### Sorting Algorithms

#### Selection Sort

This algorithm sorts an array by repeatedly finding the minimum element from the unsorted part and putting it at the end of the sorted array.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(len(A)):
    min_ind = i

    for j in range(i + 1, len(A)):
        if A[min_ind] > A[j]:
            min_ind = j

    A[i], A[min_ind] = A[min_ind], A[i]

print(A)
```

__Time Complexity__: O(n<sup>2</sup>) as there are two nested loops.

__Auxiliary Space__: O(1), the good thing about selection sort is it never makes more than O(n) swaps and can be useful when memory write is a costly operation.

#### Bubble Sort

This algorithm works by repleatedly swapping the adjacent elements of they are in wrong order.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(len(A)):
    for j in range(0, len(A) - i - 1):
        if A[j] > A[j + 1]:
            A[j], A[j + 1] = A[j + 1], A[j]
print(A)
```

__Time Complexity__: O(n<sup>2</sup>)

__Optimization__: The algorithm can be optimized by stopping the algorithm if inner loop didn't cause any swap.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(len(A)):
    swapped = False
    for j in range(0, len(A) - i - 1):
        if A[j] > A[j + 1]:
            A[j], A[j + 1] = A[j + 1], A[j]
            swapped = True
    if not swapped:
        break
print(A)
```

__Worst and Average case time complexity__: O(n<sup>2</sup>), worst case occurs when array is reverse sorted.
__Best case time complexity__: O(n)
__Auxiliary Space__: O(1)

#### Recursive bubble sort

Recursive bubble sort has the same concept, however it is coded in a recursive way.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def bubble(A, i=0):
    if len(A) - i <= 0:
        return
    for j in range(len(A) - i - 1):
        swap = False
        if A[j] > A[j + 1]:
            A[j], A[j + 1] = A[j + 1], A[j]
            swap = True
    if not swap:
        return
    bubble(A, i + 1)


bubble(A)
print(A)
```

#### Insertion sort

Insertion sort is a simple sorting algorithm that works the way we sort playing cards in out hands. We take an element and go back the already shuffeled array and insert the element where it belongs.

![](https://gyazo.com/3bcc7f4ab72658ae540085c26bd3c57b.png)

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(1, len(A)):
    cur = A[i]
    j = i - 1
    while j >= 0 and cur < A[j]:
        A[j + 1] = A[j]
        j -= 1
    A[j + 1] = cur
print(A)
```

__Time Complexity__: O(n<sup>2</sup>)

__Auxiliary Space__: O(1)

See also Recursive insertion sort.

#### Merge sort

Like Quick sort, Merge sort is a Divide and Conquer algorithm. It divides input array in two halves, calls itself for the two halfs and then merges the two halves. Merge sort can be implemented recursive or iterative.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def merge_sort(A, l, r):
    if l < r:
        m = int((l + r) / 2)
        merge_sort(A, l, m)
        merge_sort(A, m + 1, r)
        merge(A, l, m, r)


def merge(A, l, m, r):
    n_l = m + 1 - l
    n_r = r - m

    arr_l = np.copy(A[l:m+1])
    arr_r = np.copy(A[m+1:r+1])

    i_l = 0
    i_r = 0
    i_all = l
    while i_l < n_l and i_r < n_r:
        if arr_l[i_l] < arr_r[i_r]:
            A[i_all] = arr_l[i_l]
            i_l += 1
        else:
            A[i_all] = arr_r[i_r]
            i_r += 1
        i_all += 1

    for i in range(i_l, len(arr_l)):
        A[i_all] = arr_l[i]
        i_all += 1
    for i in range(i_r, len(arr_r)):
        A[i_all] = arr_r[i]
        i_all += 1


merge_sort(A, 0, len(A)-1)
print(A)
```

__Time Complexity__: With linked list O(n log n)

__Auxiliary Space__: O(n)

#### Quick Sort

Like Merge sort, Quick sort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given array around the picked pivot. There are many different versions of Quick sort that pick pivot in different ways.

1. Always pick first element as pivot.
2. Always pick last element as pivot.
3. Pick a random element as pivot.
4. Pick median as pivot.

![](https://gyazo.com/58835f6ba8e1ec8935ee37d9587414e0.png)

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def partitioning(A, low, high):
    pivot = A[high]

    i = low - 1

    for j in range(low, high):
        if A[j] <= pivot:
            i+=1
            A[i], A[j] = A[j], A[i]
    A[i + 1], A[high] = A[high], A[i + 1]
    return i + 1


def quick_sort(A, low, high):
    if low < high:
        pi = partitioning(A, low, high)

        quick_sort(A, low, pi - 1)
        quick_sort(A, pi + 1, high)


quick_sort(A, 0, len(A) - 1)
print(A)
```

#### Heap sort

The heap sort is based on binary heap data structures. It is similar to selection sort where we first find the maximum element and place the maximum element at the end. We repeat the same process for remaining element.

```python
import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def heapify(A, n, i):
    largest = i

    l = 2 * i + 1   # left = 2 * i + 1
    r = 2 * i + 2   # right = 2 * i + 2

    if l < n and A[i] < A[l]:
        largest = l

    if r < n and A[largest] < A[r]:
        largest = r

    if largest != i:
        A[i], A[largest] = A[largest], A[i]
        heapify(A, n, largest)  # heapify the root


def heap_sort(A):
    n = len(A)

    for i in range(n, -1, -1):
        heapify(A, n, i)

    for i in range(n-1, 0, -1):
        A[i], A[0] = A[0], A[i]
        heapify(A, i, 0)


heap_sort(A)
for i in range(len(A)):
    print("%d" %A[i])
```

__Time Complexity__: Time complexity of heapify is O(log n). The overall time complexity of Heap Sort is O(n log n).

__Auxiliary Space__: -

#### Counting sort

Counting sort is a sorting technique based on keys between a specific range. It works by counting the number of objects having distinct key values (kind of hashing). Then doing some arithmetic to calculate the position of each object in the output sequence.

__Time Complexity:__ O(n+k) where n is the number of elements in input array and k is the range of input.

__Auxiliary Space:__ O(n+k)

__Points to be noted:__

1. Counting sort is efficient if the range of input data is not significantly greater than the number of objects to be sorted. Consider the situation where the input sequence is between range 1 to 10K and the data is 10, 5, 10K, 5K.
1. It is not a comparison based sorting. It running time complexity is O(n) with space proportional to the range of data.
1. It is often used as a sub-routine to another sorting algorithm like radix sort.
1. Counting sort uses a partial hashing to count the occurrence of the data object in O(1).
1. Counting sort can be extended to work for negative inputs also.

#### Radix sort

Radix sort is a little more complex. Please read the concept, time complexity and auxiliary space [here](https://www.geeksforgeeks.org/radix-sort/).

```python
import numpy as np

# A function to do counting sort of arr[] according to
# the digit represented by exp.
def countingSort(A, exp1):
    n = len(A)

    # The output array elements that will have sorted arr
    output = [0] * (n)

    # initialize count array as 0
    count = [0] * (10)

    # Store count of occurrences in count[]
    for i in range(0, n):
        index = (A[i] / exp1)
        count[int(index) % 10] += 1

    # Change count[i] so that count[i] now contains actual
    #  position of this digit in output array
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Build the output array
    i = n - 1
    while i >= 0:
        index = (A[i] / exp1)
        output[count[int(index) % 10] - 1] = A[i]
        count[int(index) % 10] -= 1
        i -= 1

    # Copying the output array to arr[],
    # so that arr now contains sorted numbers
    i = 0
    for i in range(0, len(A)):
        A[i] = output[i]


# Method to do Radix Sort
def radixSort(arr):
    # Find the maximum number to know number of digits
    max1 = max(arr)

    # Do counting sort for every digit. Note that instead
    # of passing digit number, exp is passed. exp is 10^i
    # where i is current digit number
    exp = 1
    while max1 / exp > 0:
        countingSort(arr, exp)
        exp *= 10


# Driver code to test above
A = np.random.randint(low=0, high=100, size=(10))
radixSort(A)

for i in range(len(A)):
    print(A[i]),
```

### Search Algorithms

#### Linear search

#### Binary search

#### Plus search

#### Jump search

#### Interpolation search

#### Exponential Search

#### Ternary Search

Sources:

* [List of all sorting algorithms](https://www.geeksforgeeks.org/sorting-algorithms/)