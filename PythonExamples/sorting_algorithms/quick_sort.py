import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def partitioning(A, low, high):
    pivot = A[high]

    i = low - 1

    for j in range(low, high):
        if A[j] <= pivot:
            i+=1
            A[i], A[j] = A[j], A[i]
    A[i + 1], A[high] = A[high], A[i + 1]
    return i + 1


def quick_sort(A, low, high):
    if low < high:
        pi = partitioning(A, low, high)

        quick_sort(A, low, pi - 1)
        quick_sort(A, pi + 1, high)


quick_sort(A, 0, len(A) - 1)
print(A)