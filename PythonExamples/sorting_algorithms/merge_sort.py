import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def merge_sort(A, l, r):
    if l < r:
        m = int((l + r) / 2)
        merge_sort(A, l, m)
        merge_sort(A, m + 1, r)
        merge(A, l, m, r)


def merge(A, l, m, r):

    n_l = m + 1 - l
    n_r = r - m

    arr_l = np.copy(A[l:m+1])
    arr_r = np.copy(A[m+1:r+1])

    i_l = 0
    i_r = 0
    i_all = l
    while i_l < n_l and i_r < n_r:
        if arr_l[i_l] < arr_r[i_r]:
            A[i_all] = arr_l[i_l]
            i_l += 1
        else:
            A[i_all] = arr_r[i_r]
            i_r += 1
        i_all += 1

    for i in range(i_l, len(arr_l)):
        A[i_all] = arr_l[i]
        i_all += 1
    for i in range(i_r, len(arr_r)):
        A[i_all] = arr_r[i]
        i_all += 1


merge_sort(A, 0, len(A)-1)
print(A)