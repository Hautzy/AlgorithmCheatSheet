import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def heapify(A, n, i):
    largest = i

    l = 2 * i + 1   # left = 2 * i + 1
    r = 2 * i + 2   # right = 2 * i + 2

    if l < n and A[i] < A[l]:
        largest = l

    if r < n and A[largest] < A[r]:
        largest = r

    if largest != i:
        A[i], A[largest] = A[largest], A[i]
        heapify(A, n, largest)  # heapify the root


def heap_sort(A):
    n = len(A)

    for i in range(n, -1, -1):
        heapify(A, n, i)

    for i in range(n-1, 0, -1):
        A[i], A[0] = A[0], A[i]
        heapify(A, i, 0)


heap_sort(A)
for i in range(len(A)):
    print("%d" %A[i])