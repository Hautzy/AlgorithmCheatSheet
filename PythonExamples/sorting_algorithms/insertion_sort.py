import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(1, len(A)):
    cur = A[i]
    j = i - 1
    while j >= 0 and cur < A[j]:
        A[j + 1] = A[j]
        j -= 1
    A[j + 1] = cur
print(A)


