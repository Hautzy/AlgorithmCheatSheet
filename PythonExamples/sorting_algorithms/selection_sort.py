import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(len(A)):
    min_ind = i

    for j in range(i + 1, len(A)):
        if A[min_ind] > A[j]:
            min_ind = j

    A[i], A[min_ind] = A[min_ind], A[i]

print(A)

