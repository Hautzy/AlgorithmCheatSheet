import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)


def bubble(A, i=0):
    if len(A) - i <= 0:
        return
    for j in range(len(A) - i - 1):
        swap = False
        if A[j] > A[j + 1]:
            A[j], A[j + 1] = A[j + 1], A[j]
            swap = True
    if not swap:
        return
    bubble(A, i + 1)


bubble(A)
print(A)