import numpy as np

A = np.random.randint(low=0, high=100, size=(10))
print(A)

for i in range(len(A)):
    swapped = False
    for j in range(0, len(A) - i - 1):
        if A[j] > A[j + 1]:
            A[j], A[j + 1] = A[j + 1], A[j]
            swapped = True
    if not swapped:
        break
print(A)

